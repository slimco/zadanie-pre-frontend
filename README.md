# Zadanie pre Frontend

Podľa grafického návrhu vo **figme** ( https://www.figma.com/file/J8msWnW8nCUjvja3w4669D/Zadanie?node-id=0%3A1 ) alebo v priečinku **_design/** vytvorte frontend prostredie. Grafické podklady z dizajnu sú v priečinku **assets/**. Aplikácia by mala byť napísaná ideálne vo VueJS, alebo iba ES6+ s využitím npm/yarn.

Môžete využívať externé knižnice podľa vlastných preferencií, pre zobrazenie dropdown, komunikácií s API (axios) alebo routingu (Vue Router) zohľadnite ich využitie a efektívnosť. Vyhnite sa na teraz používaniu jQuery.

Všetky údaje sú fiktívne a vaša práca nebude nijakym spôsobom ďalej spenažená, slúži iba na ohodnotenie vašich znalostí.

## Prípad použitia / Use case:

Užívateľ si zvoli z ponuky značku a kategóriu. Kliknutím na vyhľadávanie sa mu zobrazia produkty podľa vybraných preferencií. Zvoliť môže značku alebo kategóriu alebo oboje.

## Vytvorte si vlastný repozitár

Naklonujte si tento repozitár a vytvorte si prosím vlastný GIT repozitár / remote branch buď na gitlab.com / github.com a nazdieľate ho s nami, môžete aj behom práce na projekte.

## Opis funkcionality

URL adresa by sa mala meniť podľa zvolených parametrov vo formáte: 

`/company/[company-slug]/category/[category-slug]/`

V dropdown by mal byť scrollbar a pekne naformátovaný ideálne aj s vyhľadávaním. 

## Komunikácia s API

Dostupné výstupne body pre komunikáciu s API 

Zobrazenie všetkých produktov.

`http://fake-api.ns.nsdevel.eu/products`

Filtrovanie podľa ID company (značky), ID category (kategórie)

`http://fake-api.ns.nsdevel.eu/products?company_id=5&category_id=5`

Zobrazenie všetkých kategórií

`http://fake-api.ns.nsdevel.eu/categories`

Zobrazenie všetkých značiek

`http://fake-api.ns.nsdevel.eu/companies`


## Vylepšenia ktoré nie je potrebné zapracovať (podľa priority):

- Browse history (funguje v Prehliadači funkcia späť)
- V dropdown ideálne aj s vyhľadávaním položiek
- Myslieť na to ak chce používateľ používať iba klávesnicu pre zvolenie značky, prípadne kategóriu a následné vyhľadávanie potvrdiť Enterom.
- Navrhnutie mobilnej verzie
- Animácie
- Lazy loading pre obrázky
- Vyladené web vitals (lighthouse chrome console)

## Hodnotí sa

- Celková práca v JavasScripte
- Grafická korektnosť a úhľadnosť
- Popisy Git commit / externé knižnice a samostnatné celky prosím commitujte jednotlivo, aby sme vedeli čo je napríklad knižnica a čo je váš ručne písaný kód.